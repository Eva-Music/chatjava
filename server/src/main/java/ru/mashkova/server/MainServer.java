package ru.mashkova.server;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MainServer {
    public static void main(String[] args) throws IOException {
        Logger globLogger = Logger.getLogger("");
        globLogger.removeHandler(globLogger.getHandlers()[0]);
        Handler handler = new FileHandler("server-log%u.log", true);
        handler.setFormatter(new SimpleFormatter());
        globLogger.addHandler(handler);

        new Server();
    }
}
