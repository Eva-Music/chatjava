package ru.mashkova.server;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLHandler {

    private static Connection connection;
    private static Statement stmt;
    private static PreparedStatement psChangeNick;
    private static PreparedStatement psGetNickByLoginAndPassword;
    private static final Logger logger = Logger.getLogger(SQLHandler.class.getName());


    public static boolean connect() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:main.db");
            stmt = connection.createStatement();
            psChangeNick = connection.prepareStatement("UPDATE users SET nickname = ? WHERE nickname = ?;");
            psGetNickByLoginAndPassword = connection.prepareStatement("SELECT nickname FROM users WHERE login = ? AND password = ?");
            return true;
        } catch (ClassNotFoundException | SQLException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            e.printStackTrace();
            return false;
        }
    }

    public static String getNicknameByLoginAndPassword(String login, String password) {
        String nick = null;
        try {
            psGetNickByLoginAndPassword.setString(1, login);
            psGetNickByLoginAndPassword.setString(2, password);
            ResultSet rs = psGetNickByLoginAndPassword.executeQuery();
            if (rs.next()) {
                nick = rs.getString(1);
            }
            rs.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Can't get nickname by login and password", e);
            e.printStackTrace();
        }
        return nick;
    }


    public static boolean changeNick(String oldNick, String newNick) {
        try {
            psChangeNick.setString(1, newNick);
            psChangeNick.setString(2, oldNick);
            psChangeNick.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Can't change nickname", e);
            e.printStackTrace();
            return false;
        }
    }

    public static void disconnect() {
        try {
            stmt.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Failed to close the connection", e);
            e.printStackTrace();
        }
        try {
            psChangeNick.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Failed to close the connection", e);
            e.printStackTrace();
        }
        try {
            psGetNickByLoginAndPassword.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Failed to close the connection", e);
            e.printStackTrace();
        }
        try {
            connection.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Failed to close the connection", e);
            e.printStackTrace();
        }
    }
}
