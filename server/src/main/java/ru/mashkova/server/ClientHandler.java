package ru.mashkova.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientHandler {
    private Server server;
    private Socket socket;
    private DataOutputStream out;
    private DataInputStream in;
    private String nickname;

    private static final Logger logger = Logger.getLogger(ClientHandler.class.getName());


    public ClientHandler(Server server, Socket socket) throws IOException {

        this.server = server;
        this.socket = socket;
        this.in = new DataInputStream(socket.getInputStream());
        this.out = new DataOutputStream(socket.getOutputStream());

        new Thread(() -> {
            try {
                while (!checkAuth()) ;

                while (readMessage()) ;
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.getMessage(), e);
                e.printStackTrace();
            } finally {
                this.disconnect();
            }
        }).start();
    }

    public String getNickname() {
        return nickname;
    }

    public void sendMsg(String msg) {
        try {
            out.writeUTF(msg);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to send message", e);
            e.printStackTrace();
        }
    }

    private void disconnect() {
        server.unsubscribe(this);
        try {
            in.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to close the connection", e);
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to close the connection", e);
            e.printStackTrace();
        }
        try {
            socket.close();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to close the connection", e);
            e.printStackTrace();
        }
    }

    private boolean checkAuth() throws IOException {
        String msg = in.readUTF();
        if (msg.startsWith("/auth ")) {
            String[] tokens = msg.split("\\s");
            String nick = server.getAuthService()
                    .getNicknameByLoginAndPassword(tokens[1], tokens[2]);
            if (nick != null && !server.isNickBusy(nick)) {
                sendMsg("/authok " + nick + " " + tokens[1]);
                nickname = nick;
                server.subscribe(this);
                return true;
            }
        }
        return false;
    }

    private boolean readMessage() throws IOException {
        String msg = in.readUTF();
        if (!msg.startsWith("/")) {
            server.broadcastMsg(nickname + ": " + msg);
            return true;
        }

        if (msg.equals("/end")) {
            sendMsg("/end");
            return false;
        }
        if (msg.startsWith("/w ")) {
            String[] tokens = msg.split("\\s", 3);
            server.privateMsg(this, tokens[1], tokens[2]);
        }
        if (msg.startsWith("/changenick ")) {
            String[] tokens = msg.split("\\s", 2);
            if (tokens[1].contains(" ")) {
                sendMsg("New nickname cannot contains spaces in it");
                return true;
            }
            if (server.getAuthService().changeNick(this.nickname, tokens[1])) {
                sendMsg("/yournick " + tokens[1]);
                sendMsg("Your nickname has changed to " + tokens[1]);
                this.nickname = tokens[1];
                server.broadcastClientsList();
            } else {
                sendMsg("Sorry, such nickname already exists, try another one");
            }
        }
        return true;
    }
}
