package ru.mashkova.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {

    private static final Logger logger = Logger.getLogger(Server.class.getName());
    private Vector<ClientHandler> clients;
    private AuthService authService;

    public Server() {
        clients = new Vector<>();
        if (!SQLHandler.connect()) {
            throw new RuntimeException("Failed to connect to DataBase");
        }
        authService = new DBAuthService();
        try (ServerSocket serverSocket = new ServerSocket(8189)) {
            logger.log(Level.INFO, "Server is working on port 8189");
            while (true) {
                Socket socket = serverSocket.accept();
                try {
                    new ClientHandler(this, socket);
                    logger.log(Level.INFO, "New client connected");
                } catch (IOException e) {
                    logger.log(Level.WARNING, "Attempt to create ClientHandler was failed!", e);
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            e.printStackTrace();
        } finally {
            SQLHandler.disconnect();
        }
        logger.log(Level.INFO, "Server closed");
    }

    public AuthService getAuthService() {
        return authService;
    }

    public void broadcastMsg(String msg) {
        for (ClientHandler client : clients) {
            client.sendMsg(msg);
        }
    }

    public void privateMsg(ClientHandler sender, String recieverNick, String msg) {
        if (sender.getNickname().equals(recieverNick)) {
            sender.sendMsg("Me to me: " + msg);
            return;
        }
        for (ClientHandler client : clients) {
            if (client.getNickname().equals(recieverNick)) {
                client.sendMsg("From " + sender.getNickname() + ": " + msg);
                sender.sendMsg("to " + recieverNick + ": " + msg);
                return;
            }
        }
        sender.sendMsg("Client " + recieverNick + " not found");
    }

    public void subscribe(ClientHandler clientHandler) {
        clients.add(clientHandler);
        broadcastClientsList();
    }

    public void unsubscribe(ClientHandler clientHandler) {

        clients.remove(clientHandler);
        broadcastClientsList();
    }

    public boolean isNickBusy(String nick) {
        for (ClientHandler client : clients) {
            if (client.getNickname().equals(nick)) {
                return true;
            }
        }
        return false;
    }

    public void broadcastClientsList() {
        StringBuilder sb = new StringBuilder(15 * clients.size());
        sb.append("/clients ");
        for (ClientHandler client : clients) {
            sb.append(client.getNickname()).append(" ");
        }
        sb.setLength(sb.length() - 1);
        String out = sb.toString();
        for (ClientHandler client : clients) {
            client.sendMsg(out);
        }
    }
}
